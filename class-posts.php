<?php
/**
 * Plugin Name:       Create Posts 
 * Plugin URI:        http://kanzucode.com/
 * Description:       Custom plugin that creates a post on activation
 * Version:           1.0.0
 * Author:            Evelyn Kikie
 * Text Domain:       testplug
 */
 
namespace Kanzu\Posts;

defined( 'ABSPATH' ) or die( 'hey u cant access');

/**
 * Class Posts
 *
 * Main Plugin class
 */
class Posts
{

    /**
     * Instance
     *
     * @access private
     * @static
     *
     * @var Posts The single instance of the class.
     */
    private static $_instance = null;
    
    /**
     * Instance
     *
     * Ensures only one instance of the class is loaded or can be loaded.
     *
     * @return Posts An instance of the class.
     * @access public
     *
     */
    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Include Plugin files
     *
     * Register Plugin Required Files
     *
     * @access public
     */
    public function register_includes(){
        require_once ( POSTS_PLUGIN_DIR . '/includes/class-scripts.php');
        require_once ( POSTS_PLUGIN_DIR . '/includes/posts/class-custom-posts.php');
        require_once ( POSTS_PLUGIN_DIR . '/includes/class-hook-registry.php');
    }

    /**
     * Plugin Constants
     *
     * Register plugin required constants
     *
     * @access public
     */
    function define_constants(){
        define('POSTS_PLUGIN_DIR', __DIR__ );
        define('POSTS_PLUGIN_FILE', __FILE__ );
        define('POSTS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
    }

    /**
     *  Plugin class constructor
     *
     * Register plugin action hooks and filters
     *
     * @access public
     */
    public function __construct(){
        
        //Define Constants
        $this->define_constants();

        //Register Includes
        $this->register_includes();
    }
}

// Instantiate Plugin Class
Posts::instance();
