<?php 

namespace Kanzu\Posts;

class Hook_Registry{

    function __construct(){
        $this->register_hooks();
    }

    function register_hooks(){
        $scripts = new Scripts();
        $custom_posts = new Custom_Posts();

        //Enqueue Styles and Scripts
        add_action( 'admin_enqueue_scripts', [ $scripts, 'register_scripts' ] );

        //Register email sending schedules for email templates
        register_activation_hook( POSTS_PLUGIN_FILE, [ $custom_posts, 'cp_create_post' ] );

        add_filter('custom_filter_for_post', [ $custom_posts, 'update_post_content' ] );
        add_filter('custom_filter_for_post', [ $custom_posts, 'update_post_content_2' ], 11, 1 );
    }
}

new Hook_Registry();
