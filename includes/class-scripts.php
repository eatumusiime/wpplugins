<?php 

namespace Kanzu\Posts;

class Scripts{

    public function register_scripts(){
        $this->enqueue_styles();
        $this->enqueue_scripts();
    }

    public function enqueue_styles(){
        wp_enqueue_style('kc-posts-css', POSTS_PLUGIN_URL.'/assets/css/styles.css' );
    }

    public function enqueue_scripts(){
        wp_enqueue_script('kc-posts-js', POSTS_PLUGIN_URL.'/assets/js/scripts.js' );
    }
   
}
