<?php 

namespace Kanzu\Posts;

class Custom_Posts{

    function cp_create_post(){
        $post = array(     
            'post_content'   => apply_filters('custom_filter_for_post', 'Test'),//__( 'this is my post content. Enjoy', 'testplug'), //content of page
            'post_title'     =>'Happy-post', //title of page
            'post_status'    =>  'publish' , //status of page - publish or draft
            'post_type'      =>  'post'  // type of post
        );

        // creates page
        wp_insert_post( $post ); 
    }

    function update_post_content( $post_content ){
        return 'Prefixed ' . $post_content . ' Adjusted.';
    }

    function update_post_content_2( $post_content ){
        return 'Prefixed ' . $post_content . ' Adjusted 2';
    }
}